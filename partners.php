<?php include('./header.php');?>

<!-- Partners(Team members) Section START -->
<div class="section-block-grey">
  <div class="container">
    <div class="section-heading center-holder">
      <span>Our partners</span>
      <h3>Collaborations</h3>
    </div>
    <div class="row mt-60">
    <div class="col-md-6 col-sm-6 col-12">
        <div class="partner-box">
          <div class="row">
            <div class="col-md-6 col-sm-12 col-12">
              <div class="partner-img">
                <img src="img/partners/scalarx.jpg" alt="scalarx logo">
              </div>
            </div>
            <div class="col-md-6 col-sm-12 col-12">
              <div class="partner-text">
                <span>Company</span>
                <h4>ScalarX</h4>
                <p>ScalarX is an international company specialized in the research, development and application of 
                innovative technological solutions in the fields of technologies and sciences. This is our official support.</p>
                  <div class="blog-grid-simple-content">
                    <div class="center-holder">
                      <a href="https://scalarx.com/">Go to website</a>
                    </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-6 col-sm-6 col-12">
        <div class="partner-box">
          <div class="row">
            <div class="col-md-6 col-sm-12 col-12">
              <div class="partner-img">
                <img src="img/partners/ldnr.jpg" alt="LDNR formation logo">
              </div>
            </div>
            <div class="col-md-6 col-sm-12 col-12">
              <div class="partner-text">
                <span>Training center</span>
                <h4>LDNR formation</h4>
                <p>LDNR formation is a french training center to form professional IT technicians, system and network
                   administrators. Their students are trained on Kaisen Linux, and we provide them with personalized ISOS adapted to their training.</p>
                   <div class="blog-grid-simple-content">
                      <div class="center-holder">
                        <a href="https://www.ldnr.fr">Go to website</a>
                      </div>
                    </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    <!--  <div class="col-md-6 col-sm-6 col-12">
        <div class="partner-box">
          <div class="row">
            <div class="col-md-6 col-sm-12 col-12">
              <div class="partner-img">
                <img src="img/partners/hackersploit.jpg" alt="hackersploit logo">
              </div>
            </div>
            <div class="col-md-6 col-sm-12 col-12">
              <div class="partner-text">
                <span>Company</span>
                <h4>HackerSploit</h4>
                <p>HackerSploit is the leading provider of free Infosec and cybersecurity training. We are their partner 
                  for the creation of their distribution based on Kaisen Linux. 
                  We help them with infrastructure, packaging and development.</p>
                  <div class="blog-grid-simple-content">
                    <div class="center-holder">
                      <a href="https://hackersploit.org/">Go to website</a>
                    </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div> -->

    </div>
  </div>
</div>
<!-- Partners(Team members) Section END -->

<?php include('./footer.php');?>
